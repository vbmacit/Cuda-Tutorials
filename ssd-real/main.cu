/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <SDL/SDL.h>
#include <float.h>
#include <time.h>
#define CREATOR "RPFELGUEIRAS"
#define RGB_COMPONENT_COLOR 255
struct pixel {
	int width;
	double *pix1;
	double *pix2;
	int *rdis;
	pixel(double *p1, double *p2, int *rd, int x) {
		pix1 = p1;
		pix2 = p2;
		width = x;
		rdis = rd;
	}
	uint8_t p1(int x, int y) {
		return pix1[x + y * width];
	}
	uint8_t p2(int x, int y) {
		return pix2[x + y * width];
	}
	void p1(int x, int y, uint8_t val) {
		pix1[x + y * width] = val;
	}
	void p2(int x, int y, uint8_t val) {
		pix2[x + y * width] = val;
	}
	void p1(int x, int y, double val, uint8_t dum) {
		pix1[x + y * width] += (val * dum);
	}
	void p2(int x, int y, double val, uint8_t dum) {
		pix2[x + y * width] += (val * dum);
	}
	int rd(int x, int y) {
		return rdis[x + y * width];
	}
	void rd(int x, int y, int val) {
		rdis[x + y * width] = val;
	}
};

int perssd(const char *filename, const char *filename2, int square,
		int border) {
	char buff[16], buff2[16];
	double *pix, *pix2;
	int x, y, *rdis;
	uint8_t dummy;
	FILE *fp, *fp2, *dos;
	int c, c2, rgb_comp_color, rgb_comp_color2;
	//open PPM file for reading
	fp = fopen(filename, "rb");
	dos = fopen("results2.pgm", "w");
	fp2 = fopen(filename2, "rb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		return -1;
	}

	//read image format
	if (!fgets(buff, sizeof(buff), fp)) {
		perror(filename);
		return -1;
	}
	//read image format
	if (!fgets(buff2, sizeof(buff2), fp2)) {
		perror(filename2);
		return -1;
	}

	//check the image format
	if (buff[0] != 'P' || buff[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		return -1;
	}
	//check the image format
	if (buff2[0] != 'P' || buff2[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		return -1;
	}

	//check for comments
	c = getc(fp);
	while (c == '#') {
		while (getc(fp) != '\n')
			;
		c = getc(fp);
	}
	c2 = getc(fp2);
	while (c2 == '#') {
		while (getc(fp2) != '\n')
			;
		c2 = getc(fp2);
	}

	ungetc(c, fp);
	//read image size information
	if (fscanf(fp, "%d %d", &x, &y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		return -1;
	}
	ungetc(c2, fp2);
	//read image size information
	if (fscanf(fp2, "%d %d", &x, &y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename2);
		return -1;
	}
	printf("sizes %d %d \n", x, y);
	//getchar();
	//asilwidth=x;
	//read rgb component
	if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		return -1;
	}
	//read rgb component
	if (fscanf(fp2, "%d", &rgb_comp_color2) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		return -1;
	}

	//check rgb component depth
	if ((rgb_comp_color2 != RGB_COMPONENT_COLOR)
			|| (rgb_comp_color != RGB_COMPONENT_COLOR)) {
		fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
		return -1;
	}

	while (fgetc(fp) != '\n')
		;
	while (fgetc(fp2) != '\n')
		;
	//memory allocation for pixel data
	pix = (double *) malloc(x * y * sizeof(double));
	pix2 = (double *) malloc(x * y * sizeof(double));
	rdis = (int *) malloc(
			(x - (square - 1)) * (y - (square - 1)) * sizeof(int));
	if (!pix || !pix2) {
		fprintf(stderr, "Unable to allocate memory\n");
		return -1;
	}
	struct pixel px(pix, pix2, rdis, x);

	for (int j = 0; j < y; j++) {
		for (int i = 0; i < x; i++) {
			px.p1(i, j, 0);
			px.p2(i, j, 0);
			fread(&dummy, 1, sizeof(uint8_t), fp);
			px.p1(i, j, 0.2989, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp);
			px.p1(i, j, 0.5870, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp);
			px.p1(i, j, 0.1140, dummy);

			fread(&dummy, 1, sizeof(uint8_t), fp2);
			px.p2(i, j, 0.2989, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp2);
			px.p2(i, j, 0.5870, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp2);
			px.p2(i, j, 0.1140, dummy);

		}
	}
	int bas, son;
	double ssd = 66500 * square * square, temp;
	for (int j = 0; j < y - (square - 1); j++) {
		for (int i = 0; i < x - (square - 1); i++) {

			ssd = 66500 * square * square;
			if (border > 0) {
				if ((square - 1) / 2 >= (i - 100))
					bas = 0;
				else
					bas = (i - 100);
				if ((x - ((square - 1) / 2)) <= (i + 100))
					son = x - (square - 1);
				else
					son = (i + 100);
			} else {
				bas = 0;
				son = x - (square - 1);
			}
			for (int ww = bas; ww < son; ww++) {
				temp = 0;
				for (int sy = 0; sy < square; sy++) {
					for (int sx = 0; sx < square; sx++) {
						temp += (px.p1(i + sx, j + sy) - px.p2(ww + sx, j + sy))
								* (px.p1(i + sx, j + sy)
										- px.p2(ww + sx, j + sy));
					}
				}
				if (ssd > temp) {
					ssd = temp;
					px.rd(i, j, ww - i);

				}

			}
		}
	}

	int max = -999999, min = +999999;
	if (border > 0) {
		for (int i = 0; i < (x - (square - 1)); i++) {
			for (int j = 0; j < (y - (square - 1)); j++) {
				if (max < px.rd(i, j))
					max = px.rd(i, j);
				if (min > px.rd(i, j))
					min = px.rd(i, j);
			}
		}
		if (abs(min) > max)
			max = -min;
	} else {
		max = border;
	}

	fprintf(dos, "P2\n%d %d\n%d\n", x - (square - 1), y - (square - 1), max);
	for (int j = 0; j < (y - (square - 1)); j++) {
		for (int i = 0; i < (x - (square - 1)); i++) {
			if (i % (x - (square - 1)) == 0 && i != 0)
				fprintf(dos, "\n");
			if (px.rd(i, j) < 0)
				px.rd(i, j, -px.rd(i, j));
			fprintf(dos, "%d ", px.rd(i, j));
		}
	}

	printf("max:%d min:%d\n", max, min);
	fclose(fp);
	fclose(fp2);
	fclose(dos);
	return 0;
}

int cudassd(const char *filename, const char *filename2, int square,
		int border) {
	char buff[16], buff2[16];
	double *pix, *pix2;
	int x, y, *rdis;
	uint8_t dummy;
	FILE *fp, *fp2, *dos;
	int c, c2, rgb_comp_color, rgb_comp_color2;
	//open PPM file for reading
	fp = fopen(filename, "rb");
	dos = fopen("results2.pgm", "w");
	fp2 = fopen(filename2, "rb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		return -1;
	}

	//read image format
	if (!fgets(buff, sizeof(buff), fp)) {
		perror(filename);
		return -1;
	}
	//read image format
	if (!fgets(buff2, sizeof(buff2), fp2)) {
		perror(filename2);
		return -1;
	}

	//check the image format
	if (buff[0] != 'P' || buff[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		return -1;
	}
	//check the image format
	if (buff2[0] != 'P' || buff2[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		return -1;
	}

	//check for comments
	c = getc(fp);
	while (c == '#') {
		while (getc(fp) != '\n')
			;
		c = getc(fp);
	}
	c2 = getc(fp2);
	while (c2 == '#') {
		while (getc(fp2) != '\n')
			;
		c2 = getc(fp2);
	}

	ungetc(c, fp);
	//read image size information
	if (fscanf(fp, "%d %d", &x, &y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		return -1;
	}
	ungetc(c2, fp2);
	//read image size information
	if (fscanf(fp2, "%d %d", &x, &y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename2);
		return -1;
	}
	printf("sizes %d %d \n", x, y);
	//getchar();
	//asilwidth=x;
	//read rgb component
	if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		return -1;
	}
	//read rgb component
	if (fscanf(fp2, "%d", &rgb_comp_color2) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		return -1;
	}

	//check rgb component depth
	if ((rgb_comp_color2 != RGB_COMPONENT_COLOR)
			|| (rgb_comp_color != RGB_COMPONENT_COLOR)) {
		fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
		return -1;
	}

	while (fgetc(fp) != '\n')
		;
	while (fgetc(fp2) != '\n')
		;
	//memory allocation for pixel data
	pix = (double *) malloc(x * y * sizeof(double));
	pix2 = (double *) malloc(x * y * sizeof(double));
	rdis = (int *) malloc(
			(x - (square - 1)) * (y - (square - 1)) * sizeof(int));
	if (!pix || !pix2) {
		fprintf(stderr, "Unable to allocate memory\n");
		return -1;
	}
	struct pixel px(pix, pix2, rdis, x);

	for (int j = 0; j < y; j++) {
		for (int i = 0; i < x; i++) {
			px.p1(i, j, 0);
			px.p2(i, j, 0);
			fread(&dummy, 1, sizeof(uint8_t), fp);
			px.p1(i, j, 0.2989, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp);
			px.p1(i, j, 0.5870, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp);
			px.p1(i, j, 0.1140, dummy);

			fread(&dummy, 1, sizeof(uint8_t), fp2);
			px.p2(i, j, 0.2989, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp2);
			px.p2(i, j, 0.5870, dummy);
			fread(&dummy, 1, sizeof(uint8_t), fp2);
			px.p2(i, j, 0.1140, dummy);

		}
	}
	int bas, son;
	double ssd = 66500 * square * square, temp;
	for (int j = 0; j < y - (square - 1); j++) {
		for (int i = 0; i < x - (square - 1); i++) {

			ssd = 66500 * square * square;
			if (border > 0) {
				if ((square - 1) / 2 >= (i - 100))
					bas = 0;
				else
					bas = (i - 100);
				if ((x - ((square - 1) / 2)) <= (i + 100))
					son = x - (square - 1);
				else
					son = (i + 100);
			} else {
				bas = 0;
				son = x - (square - 1);
			}
			for (int ww = bas; ww < son; ww++) {
				temp = 0;
				for (int sy = 0; sy < square; sy++) {
					for (int sx = 0; sx < square; sx++) {
						temp += (px.p1(i + sx, j + sy) - px.p2(ww + sx, j + sy))
								* (px.p1(i + sx, j + sy)
										- px.p2(ww + sx, j + sy));
					}
				}
				if (ssd > temp) {
					ssd = temp;
					px.rd(i, j, ww - i);

				}

			}
		}
	}

	int max = -999999, min = +999999;
	if (border > 0) {
		for (int i = 0; i < (x - (square - 1)); i++) {
			for (int j = 0; j < (y - (square - 1)); j++) {
				if (max < px.rd(i, j))
					max = px.rd(i, j);
				if (min > px.rd(i, j))
					min = px.rd(i, j);
			}
		}
		if (abs(min) > max)
			max = -min;
	} else {
		max = border;
	}

	fprintf(dos, "P2\n%d %d\n%d\n", x - (square - 1), y - (square - 1), max);
	for (int j = 0; j < (y - (square - 1)); j++) {
		for (int i = 0; i < (x - (square - 1)); i++) {
			if (i % (x - (square - 1)) == 0 && i != 0)
				fprintf(dos, "\n");
			if (px.rd(i, j) < 0)
				px.rd(i, j, -px.rd(i, j));
			fprintf(dos, "%d ", px.rd(i, j));
		}
	}

	printf("max:%d min:%d\n", max, min);
	fclose(fp);
	fclose(fp2);
	fclose(dos);
	return 0;
}

int main(int argc, char **argv) {
	//clock_t time=clock();
	perssd("/developments/workspace/envoirment/Debug/right.ppm",
			"/developments/workspace/envoirment/Debug/left.ppm", 5, 100);
	//printf("\nTotal Time:%f\n",((float)clock()-(float)time)/CLOCKS_PER_SEC);
	return 0;
}

