/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#define X 640
#define Y 480
#define S 5
struct hpix{
	uint8_t *pix1,*pix2;
	int *disp;
	int width;

	hpix(uint8_t *p1,uint8_t *p2,int *d,int x){
		pix1=p1;
		pix2=p2;
		disp=d;
		width=x;
	}

	void p1(int x,int y,uint8_t val){
		pix1[x+y*width]=val;
	}
	void p2(int x,int y,uint8_t val){
		pix2[x+y*width]=val;
	}

};
struct dpix{
	uint8_t *pix1;
	int *disp;
	int width;

	__device__ dpix(uint8_t *p,int *d,int x){
		pix1=p;
		disp=d;
		width=x;
	}

	__device__ void p1(int x,int y,uint8_t val){
		pix1[x+y*width]=val;
	}


};

__device__ void pw(uint_fast8_t *pix,int x,int y,int width,uint_fast8_t val){
		pix[x+y*width]=val;
}
__device__ uint_fast8_t pr(uint_fast8_t *pix,int x,int y,int width){
		return pix[x+y*width];
}
__device__ uint16_t s2(uint_fast8_t val){
	return val*val;
}
__device__ void mw(uint_fast32_t *mat,uint_fast16_t x,uint_fast16_t y,uint_fast16_t width,uint_fast16_t val){
	mat[x+width*y]=val;
}
__global__ void ksumom(uint_fast32_t *mat,unsigned int total){

}
__global__ void kssd(uint_fast8_t *pix1,uint_fast8_t *pix2,int *dis,int x,int y,const int s,int border){
	int cx=blockIdx.x;
	int cy=blockIdx.y;
	int ctx=threadIdx.x;
	int cty=threadIdx.y;
	extern __shared__ uint_fast16_t matrix[];

	//__shared__ matrix=(uint_fast16_t *)malloc(sizeof(uint_fast16_t)*s*s);
	if(ctx<s&&cty<s&&cx<(x-s+1)&&cy<(y-s+1)){
		int bas,son;
		if (border > 0) {
			if ((s - 1) / 2 >= (cx - 100))
				bas = 0;
			else
				bas = (cx - 100);
			if ((x - ((s - 1) / 2)) <= (cx + 100))
				son = x - (s - 1);
			else
				son = (cx + 100);
		} else {
			bas = 0;
			son = x - (s - 1);
		}
		unsigned int cssd=255*255*s*s;
		for(int slide=bas;slide<son;slide++){
		s2(pr(pix1,cx,cy,x)-pr(pix2,slide,cy,x));


		}

	}

}


int main(int argc, char **argv) {
	dim3 blocks(X-S+1,Y-S+1);
	dim3 threads(5,5);
	uint_fast8_t *pix1,*pix2,*pixd1,*pixd2;
	int *dis,*disd;
	const int ss=S;
	pix1=(uint_fast8_t *)malloc(sizeof(uint_fast8_t)*X*Y);
	pix2=(uint_fast8_t *)malloc(sizeof(uint_fast8_t)*X*Y);
	dis=(int *)malloc(sizeof(int)*(X-(S-1)/2)*(Y-(S-1)/2));
	cudaMalloc((void **)&pixd1,sizeof(uint_fast8_t)*X*Y);
	cudaMalloc((void **)&pixd2,sizeof(uint_fast8_t)*X*Y);
	cudaMalloc((void **)&disd,sizeof(int)*(X-(S-1)/2)*(Y-(S-1)/2));
	struct hpix hp(pix1,pix2,dis,X);

	for(int i=0;i<X;i++)
		for(int j=0;j<Y;j++){
			hp.p1(i,j,(i*j)%250);
			hp.p2(i,j,(i*j)%125);
		}



	cudaMemcpy(pix1,pixd1,sizeof(uint_fast8_t)*X*Y,cudaMemcpyHostToDevice);
	cudaMemcpy(pix2,pixd2,sizeof(uint_fast8_t)*X*Y,cudaMemcpyHostToDevice);

	kssd<<<blocks,threads,S*S>>>(pixd1,pixd2,disd,X,Y,ss,100);
	//ksumom<<<blocks,threads>>>(NULL,NULL);
	cudaFree(pixd1);
	cudaFree(pixd2);
	cudaFree(disd);


}

