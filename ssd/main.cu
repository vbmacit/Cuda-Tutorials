/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <float.h>

/////////////////
#define RGB_COMPONENT_COLOR 255

///////////////////

__host__ int perncc(const char *filename,const char *filename2,int square,unsigned int border) {
	//int asilwidth=0;
	char buff[16],buff2[16];
	unsigned char **pix,**pix2;
	int x ,y,**rdis;
	uint8_t dummy;
	double dummy2;
	FILE *fp,*fp2,*dos;
	int c,c2 ,rgb_comp_color,rgb_comp_color2;
	//open PPM file for reading
	fp = fopen(filename, "rb");
	dos=fopen("results2.pgm","w");
	fp2 = fopen(filename2, "rb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		return -1;
	}

	//read image format
	if (!fgets(buff, sizeof(buff), fp)) {
		perror(filename);
		return -1;
	}
	//read image format
	if (!fgets(buff2, sizeof(buff2), fp2)) {
		perror(filename2);
		return -1;
	}


	//check the image format
	if (buff[0] != 'P' || buff[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		return -1;
	}
	//check the image format
	if (buff2[0] != 'P' || buff2[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		return -1;
	}


	//check for comments
	c = getc(fp);
	while (c == '#') {
		while (getc(fp) != '\n');
		c = getc(fp);
	}
	c2 = getc(fp2);
	while (c2 == '#') {
		while (getc(fp2) != '\n');
		c2 = getc(fp2);
	}

	ungetc(c, fp);
	//read image size information
	if (fscanf(fp, "%d %d", &x, &y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		return -1;
	}
	ungetc(c2, fp2);
	//read image size information
	if (fscanf(fp2, "%d %d", &x, &y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename2);
		return -1;
	}
	printf("sizes %d %d \n",x,y);//x=width,y=height
	//getchar();
	//asilwidth=x;
	//read rgb component
	if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		return -1;
	}
	//read rgb component
	if (fscanf(fp2, "%d", &rgb_comp_color2) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		return -1;
	}


	//check rgb component depth
	if ((rgb_comp_color2 != RGB_COMPONENT_COLOR)||(rgb_comp_color != RGB_COMPONENT_COLOR)) {
		fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
		return -1;
	}

	while (fgetc(fp) != '\n');
	while (fgetc(fp2) != '\n');
	//memory allocation for pixel data
	pix =  (unsigned char **) malloc(x*sizeof(unsigned char *));
	pix2 = (unsigned char **) malloc(x*sizeof(unsigned char *));
	//rdis = (int **) malloc((x-(square-1))*(y-(square-1))  * sizeof(int *));
	rdis=(int **) malloc((x-(square-1))* sizeof(int *));
	if (!pix||!pix2) {
		fprintf(stderr, "Unable to allocate memory\n");
		return -1;
	}
	for(int ii=0;ii<y;ii++){
		pix [i]=(unsigned char *)malloc(sizeof(unsigned char)*y);
		pix2[i]=(unsigned char *)malloc(sizeof(unsigned char)*y);
	}

		for(int j=0;j<y;j++){

			for (int i = 0; i <x; i++) {

		dummy2=0;
		fread(&dummy, 1, sizeof(uint8_t), fp);
		dummy2+=0.2989*dummy;
		fread(&dummy, 1, sizeof(uint8_t), fp);
		dummy2+=0.5870*dummy;
		fread(&dummy, 1, sizeof(uint8_t), fp);
		dummy2+=0.1140*dummy;
		pix[i][j]=dummy2;

		dummy2=0;
		fread(&dummy, 1, sizeof(uint8_t), fp2);
		dummy2+=0.2989*dummy;
		fread(&dummy, 1, sizeof(uint8_t), fp2);
		dummy2+=0.5870*dummy;
		fread(&dummy, 1, sizeof(uint8_t), fp2);
		dummy2+=0.1140*dummy;
		pix2[i][j]=dummy2;

		}
	}
	/////////////////////
	/*
	int max=-999999,min=+999999;
	for(int i=0;i<x*y;i++){
	if(max<pix[i]) max=pix[i];
	if(min>pix[i]) min=pix[i];
	}
	fprintf(dos, "P2\n%d %d\n%d\n", x, y,max-min);
	for(int i=0;i<x*y;i++){
	if(i%x==0) fprintf(dos,"\n");
	fprintf(dos,"%d ",(int)pix[i]-min);
	}
	*/
	/////////////////////////////
	///here///
	 int say=0,bas,son;
	double ncc,tempalt1,tempalt2,tempust,ort1,ort2;
	//int disparity;
	/*
	for(int j=(square-1)/2;j<y-(square-1)/2;j++){
		for(int i=(square-1)/2;i<x-(square-1)/2;i++){
		*/
		for(int i=0;i<x-square;i++){
			rdis[i]=(int *)malloc(sizeof(int)*(y-(square-1)));
			for(int j=0;j<y-square;j++){

				ncc=-DBL_MAX;
				rdis[i][j]=-99999999;
				ort1=0;
				for(int hy=0;hy<square;hy++)
					for(int hx=0;hx<square;hx++)
						//ort1+=pix[ (i-(square-1)/2)+x*(j-(square-1)/2)+hx+hy*x ];
						ort1+=pix[i+hx][j+hy];
						ort1=ort1/(square*square);
					if(border=!0){
			    	if(square>=(i-border)) bas=0;//(square-1)/2;
					else					   bas=(i-border);
					if((x-square)<=(i+border)) son=x-square;//(square-1)/2;
					else					   son=(i+border);
					}else{
						bas=0;
						son=x-square;
					}
					for(int ww=bas;ww<son ;ww++){

					ort2=tempalt1=tempalt2=tempust=0;
					for(int hy1=0;hy1<square;hy1++)
						for(int hx1=0;hx1<square;hx1++)
							//ort2+=pix2[(ww-(square-1)/2)+x*(j-(square-1)/2)+hx1+hy1*x];
							ort2+=pix2[ww+hx1][j+hy1];
							ort2=ort2/(square*square);

					for(int sy=0;sy<square;sy++){
						for(int sx=0;sx<square;sx++){
							tempust+=(pix[i+sx][j+sy]-ort1)*(pix2[ww+sx][j+sy]-ort2);
							tempalt1+=(pix[i+sx][j+sy]-ort1)*(pix[i+sx][j+sy]-ort1);
							tempalt2+=(pix2[ww+sx][j+sy]-ort2)*(pix2[ww+sx][j+sy]-ort2);
							//printf("pix:%f--%d    pix2:%f--%d say:%d\n",pix[ (i-(square-1)/2)+x*(j-(square-1)/2)+sx+sy*x ],i-(square-1)/2+x*(j-(square-1)/2)+sx+sy*x,pix2[(ww-(square-1)/2)+x*(j-(square-1)/2)+sx+sy*x],ww-(square-1)/2+x*(j-(square-1)/2)+sx+sy*x,say);
							//getchar();
							//	temp+=(oku(pix,i-(square-1)/2),))
							//printf("say:%d\n",say);
						}
						}
					//printf("------------->temp:%f\n",temp);
					if(ncc<tempust/sqrt(tempalt1*tempalt2)){
						ncc=tempust/sqrt(tempalt1*tempalt2);
					//disparity=ww-i;
					rdis[i][j]=ww-i;


					}

				}

				//fprintf(dos,"%d ",disparity);
				//puts("hehe");
				//printf("-->w:%d (x+dis)=%d x=%d y=%d disparity=%d ssd:%f ssp:%f\n",x,disparity+i,i,j,disparity,ssd,sqrt(ssd)/(square*square));
				//getchar();
					if(say%(5000/square)==0)
			printf("say[%d]:%d Process:%f\n",say,rdis[i][j],((float)say/((float)x*(float)y))*100);
			//getchar();
				/*
					say++;
				if(rdis[say-1]==-99999999){
					printf("-->say[%d]:%d deger:%f-%f\n",say,rdis[say-1],(float)tempalt1,tempalt2);
					getchar();
				}
				*/
			}
	//	fprintf(dos,"\n");


	}
	puts("heheheheh");
	int max=-999999,min=+999999;
	for(int i=0;i<(x-(square-1));i++){
		for(int j=0;j<(y-(square-1));j++){
	if(max<rdis[i][j]) max=rdis[i][j];
	if(min>rdis[i][j]) min=rdis[i][j];
	//printf("say:%d i:%d\n",(x-(square-1))*(y-(square-1)),i);
	}
}
	if(abs(min)>max) max=-min;
	puts("heheheheh");
	fprintf(dos, "P2\n%d %d\n%d\n", x-(square-1), y-(square-1),max);//max-min);
	puts("heheheheh");
	for(int i=0;i<(x-(square-1));i++){
		for(int j=0;j<(y-(square-1));j++){
	if(i%(x-(square-1))==0&&i!=0) fprintf(dos,"\n");
	//fprintf(dos,"%d ",max-rdis[i]);
	if(rdis[i][j]<0) rdis[i][j]=-rdis[i][j];
	//fprintf(dos,"%d ",((max-min)-rdis[i]));
	fprintf(dos,"%d ",rdis[i]);
	}
	}

	printf("max:%d min:%d\n",max,min);
	fclose(fp);
	fclose(fp2);
	fclose(dos);
	return 0;
}
int main(){}
