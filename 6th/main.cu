/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "stdio.h"

#define X 3000
#define Y 5000
__global__ void fill(int *a,int *b){
	unsigned int tid=threadIdx.x+(blockIdx.x+blockIdx.y*gridDim.x)*blockDim.x;
	if(tid<X*Y*4){
		a[tid]=-tid;
		b[tid]=tid;
	}

}

int main(int argc, char **argv) {

	cudaDeviceProp prob;
	int device;
	cudaGetDevice(&device);
	cudaGetDeviceProperties(&prob,device);
	printf("resault:%d--%d",prob.maxThreadsPerBlock,prob.maxGridSize);

	int *a,*b,*da,*db;
	a=(int *)malloc(sizeof(int)*X*Y*4);
	b=(int *)malloc(sizeof(int)*X*Y*4);
	cudaMalloc((void **)&da,sizeof(int)*X*Y*4);
	cudaMalloc((void **)&db,sizeof(int)*X*Y*4);

	dim3 pixs(X,Y);
	fill<<<pixs,4>>>(da,db);

	cudaMemcpy(a,da,sizeof(int)*X*Y*4,cudaMemcpyDeviceToHost);
	cudaMemcpy(b,db,sizeof(int)*X*Y*4,cudaMemcpyDeviceToHost);

	for(unsigned int i=0;i<X*Y*4;i++){
		if(i%200==0)
		printf("X=%d,Y=%d\n",a[i],b[i]);
	}



	return 0;

}
