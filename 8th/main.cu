/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdio.h>

__global__ void kernel(int *a){
	//extern __shared__ int b[64];
	int index=threadIdx.x + threadIdx.y*blockDim.x;
	int x=threadIdx.x;
	int y=threadIdx.y;

	if(x<8&&y<8){

	//b[index]=a[index];
	//__syncthreads();
	a[index]=3;
	__syncthreads();
	a[index]=2;
	__syncthreads();
	//
	}
}

int main(int argc, char **argv) {
	int a[64],*b;
	for(int i=0;i<8*8;i++)
			a[i]=1;
	cudaMalloc((void **)&b,sizeof(int)*64);
	cudaMemcpy(b,a,sizeof(int)*64,cudaMemcpyHostToDevice);
	dim3 thr(8,8);
	kernel<<<1,thr>>>(b);
	cudaMemcpy(a,b,sizeof(int)*64,cudaMemcpyDeviceToHost);
	for(int i=0;i<8*8;i++)
	printf("-->%d\n",a[i]);
	return 0;
}
