/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "stdio.h"
#include <sys/time.h>

#define N 60000

__global__ void add(int *a,int *b,int *c){
	int i=blockIdx.x;
	if(i<N)
		c[i]=a[i]+b[i];
}

__global__ void fill(int *a,int *b){
	int i=blockIdx.x;
	if(i<N){
		a[i]=-i;
		b[i]=2*i;
	}
}

int main(){
	timeval t1, t2;
	double elapsedTime;

	int a[N],b[N],c[N];
	int *da,*db,*dc;

	gettimeofday(&t1, NULL);
	cudaMalloc((void **)&da,N*sizeof(int));
	cudaMalloc((void **)&db,N*sizeof(int));
	cudaMalloc((void **)&dc,N*sizeof(int));
	fill<<<N,1>>>(da,db);
	add<<<N,1>>>(da,db,dc);
	cudaMemcpy(a,da,N*sizeof(int),cudaMemcpyDeviceToHost);
	cudaMemcpy(b,db,N*sizeof(int),cudaMemcpyDeviceToHost);
	cudaMemcpy(c,dc,N*sizeof(int),cudaMemcpyDeviceToHost);
	 gettimeofday(&t2, NULL);
	    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
	    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
	printf("Zaman=%f",elapsedTime);
	getchar();
	 for(int i=0;i<N;i++){
		printf("%d+%d=%d\n",a[i],b[i],c[i]);
	}
	return 0;
}
