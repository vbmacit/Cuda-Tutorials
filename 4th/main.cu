/*
Copyright (C) 2016 Veysel Bekir Macit <brainprism90@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "stdio.h"

int main(){
	cudaDeviceProp prob;
	memset(&prob,0,sizeof(cudaDeviceProp));
	prob.major=1;
	prob.minor=3;
	int dev;
	cudaGetDevice(&dev);
	printf("current device:%d\n",dev);
	cudaChooseDevice(&dev,&prob);
	printf("like we wanted:%d\n",dev);
	cudaSetDevice(dev);
	cudaGetDeviceProperties(&prob,dev);
	printf("major:%d , minor:%d\n",prob.major,prob.minor);
	return 0;
}
